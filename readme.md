## SPN密码线性\差分分析
- 学习《A-Tutorial-on-Linear-and-Differential-Cryptanalysis》过程，将其中展示的线性分析和差分分析做了简单实现。
### 文件说明
#### 1.`algorithm.py`文件
- 加密的基本实现
- 生成10000个明密文对，并存入json文件
#### 2. `LinearAttack.py`文件
- 生成线性近似表（即论文中Table 4）
- 复现论文中的线性攻击（active sbox及线性近似的选取直接使用论文给出的例子，攻击得到最后一轮子密钥的第2和第4段）
#### 3. `DifferentialAttack.py`文件
- 生成差分分布表
- 复现论文中的差分攻击
#### 4. `Plain_Cipher_pairs.json`文件
- 记录10000组明密文对


### 线性分析测试说明
#### 1. 线性近似表的生成
- 运行`LinearAttack.py`文件，可以看到生成的一个S盒的线性近似表

![](pic/linear_table.png)
#### 2. 攻击过程测试
- 还是运行`LinearAttack.py`文件，输出目标部分子密钥和实际子密钥，对比可以发现攻击过程确实成功获得target partial subkeys
- 运行需要一段时间，请多等几秒
![](pic/linear_res.png)

#### 3. 换一组轮密钥再测试
- 先在`algorithm.py`文件中修改17行self.rk的取值，改完后运行该文件，将生成10000组明密文对并存入json文件。
  - 例如：self.rk = [0x32ab, 0x684d, 0xaa93, 0x4800, 0x463c]
- 之后直接运行`LinearAttack.py`文件即可看到攻击结果，发现仍然得到正确结果。
![](pic/linear_res2.png)

### 差分分析测试说明
- 和线性分析的操作一致，不再做描述，直接粘结果图。
![](pic/differ_table.png)
![](pic/differ_res.png)
![](pic/differ_res2.png)